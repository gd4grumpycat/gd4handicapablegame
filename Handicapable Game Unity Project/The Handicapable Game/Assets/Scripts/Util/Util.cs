﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/*
 * @author Jan Julius de Lang
 * @author Jared Nealon
 * The Grumpy Cat Team©
 */
/// <summary>
/// represents the ultility class
/// </summary>
public static class Util
{

    public static System.Random random = new System.Random();

    public static IEnumerable<T> RandomPermutation<T>(IEnumerable<T> sequence)
    {
        T[] retArray = sequence.ToArray() as T[];

        for (int i = 0; i < retArray.Length - 1; i++)
        {
            int swapIndex = random.Next(i + 1, retArray.Length);
            T temp = retArray[i];
            retArray[i] = retArray[swapIndex];
            retArray[swapIndex] = temp;
        }

        return retArray;
    }

}
