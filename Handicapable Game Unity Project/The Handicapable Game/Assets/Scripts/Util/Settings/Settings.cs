﻿using UnityEngine;
using System.Collections;

public static class Settings
{
    public enum Language
    {
        Dutch,
        English
    };

    public static Language language = Language.Dutch;
}
