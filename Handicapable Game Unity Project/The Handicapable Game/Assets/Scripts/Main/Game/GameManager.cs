﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * @author Jan Julius de Lang
 * @author Jared Nealon
 * The Grumpy Cat Team©
 */
/// <summary>
/// Handles everything within the game which needs to always be dragged over/acitve, is never destroyed
/// </summary>
public class GameManager : MonoBehaviour
{
    /// <summary>
    /// Defines possible gamemodes
    /// </summary>
    public enum GameMode
    {
        ManualWalk,
        AutoWalk
    };

    /// <summary>
    /// represents the current gamemode
    /// </summary>
    public static GameMode gameMode;


    private bool respawnTimerIsRunning;

    //player information
    [SerializeField]
    private GameObject playerObject;
    private GameObject myPlayer;

    private Player playerComponent;
    private PlayerInput playerInput;

    private int playerScore = 0;
    private int collectedItems;
    private float distanceTraveled;

    /// <summary>
    /// interfacemanager component of the interfacemanager class
    /// </summary>
    public InterfaceManager interfaceManager;

    //other info
    /// <summary>
    /// Easy scene variable
    /// </summary>
    private readonly int SCENE_MENUSCREEN = 0, SCENE_GAME = 1, SCENE_RESULTSCREEN = 2, SCENE_START = 3;

    private readonly int POPUPTYPE_MOTIVATIONAL = 0, POPUPTYPE_FAIL = 1, POPUPTYPE_COLLECTED = 2;

    //readonly player information
    /// <summary>
    /// spawnposition of player as Vector3
    /// </summary>
    private readonly Vector3 spawnPosition = new Vector3(0, 2f, 33.50f);

    public void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        LoadScene(SCENE_MENUSCREEN);
        SetScore(0);
    }

    /// <summary>
    /// loads a scene depending on scene id interger
    /// </summary>
    /// <param name="sceneId">scene id to load</param>
    public void LoadScene(int sceneId)
    {
        SceneManager.LoadScene(sceneId);
    }

    /// <summary>
    /// spawns the player on preset values
    /// </summary>
    public void SpawnPlayer()
    {
        myPlayer = Instantiate(playerObject, spawnPosition, Quaternion.identity) as GameObject;
        playerComponent = myPlayer.GetComponent<Player>();
        playerInput = myPlayer.GetComponent<PlayerInput>();
        playerComponent.SetGamemode(gameMode);
    }

    /// <summary>
    /// spawns player with custom values
    /// </summary>
    /// <param name="spawnPos">position to spawn the player at as vector3</param>
    /// <param name="spawnRotation">rotation to spawn the player at as Quaternion</param>
    /// <param name="spawnObject">object to spawn as GameObject</param>
    public void SpawnPlayer(Vector3 spawnPos, Quaternion spawnRotation, GameObject spawnObject)
    {
        myPlayer = Instantiate(spawnObject, spawnPos, spawnRotation) as GameObject;
        playerComponent = myPlayer.GetComponent<Player>();
        playerComponent.SetGamemode(gameMode);
    }

    public void Update()
    {
        if (playerComponent != null)
        {
            //generates new segment if player is 30 units further than the oldest alive segment
            if (myPlayer.transform.position.z > StraightEndlessGenerator.getOldestAliveSegment.gameObject.transform.position.z + StraightEndlessGenerator.getOldestAliveSegment.size * 1.5f)
            {
                StraightEndlessGenerator.GetGenerator().MoveToNextSegment();
                SetScore(GetScore() + 100);
                interfaceManager.UpdatePopup(POPUPTYPE_MOTIVATIONAL);
            }
            if (myPlayer.transform.position.y <= 0)
            {
                RespawnPlayer();
            }
        }
#if UNITY_EDITOR
        if (Input.GetKeyDown("5"))
        {
            RespawnPlayer();
        }
#endif
    }

    /// <summary>
    /// gets called whenever a new scene is loaded
    /// </summary>
    public void OnLevelWasLoaded()
    {
        if (Application.loadedLevelName == "Main")
        {
            SpawnPlayer();
            interfaceManager = GameObject.FindObjectOfType<InterfaceManager>();
            ResetScore();
        }
    }

    /// <summary>
    /// starts the game relative to given gamemode
    /// </summary>
    /// <param name="gm">gamemode of gamemode type enum in gamemanager</param>
    public void StartGame(GameMode gm)
    {
        SceneManager.LoadScene(SCENE_GAME);
    }

    /// <summary>
    /// starts game relative to given interger interger depends the game mode converts to gamemode type
    /// manual walk = 0
    /// auto walk = 1
    /// </summary>
    /// <param name="id">gamemode id</param>
    public void StartGame(int id)
    {
        switch (id)
        {
            case 0:
                gameMode = GameMode.ManualWalk;
                break;
            case 1:
                gameMode = GameMode.AutoWalk;
                break;
        }
        StartGame(gameMode);
    }

    /// <summary>
    /// respawns the player to the last segment
    /// </summary>
    public void RespawnPlayer()
    {
        //sets player back to oldest segment position
        myPlayer.gameObject.transform.position = new Vector3(0, StraightEndlessGenerator.getOldestAlivePosition().x + 2, StraightEndlessGenerator.getNextNodePositionZ());
        NullifyPlayerVelocity();
        StraightEndlessGenerator.GetGenerator().MoveToNextSegment();
        interfaceManager.UpdatePopup(POPUPTYPE_FAIL);

        if (!respawnTimerIsRunning)
        {
            StartCoroutine("StartRespawnTimer");
        }
        /*
#if UNITY_EDITOR
        myPlayer.GetComponent<PlayerInput>().setValueWalk(0);
#endif
         */
    }
    /// <summary>
    /// sets player velocity to 0 on all axis (makes the player stand still)
    /// </summary>
    public void NullifyPlayerVelocity()
    {
        myPlayer.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
    }

    /// <summary>
    /// locks player movement for 3 seconds incase of failing (called when respawning player)
    /// </summary>
    /// <returns></returns>
    public IEnumerator StartRespawnTimer()
    {
        respawnTimerIsRunning = true;
        myPlayer.GetComponent<Player>().enabled = false;
        myPlayer.GetComponent<PlayerInput>().enabled = false;
        yield return new WaitForSeconds(1);
        myPlayer.GetComponent<Player>().enabled = true;
        myPlayer.GetComponent<PlayerInput>().enabled = true;
        respawnTimerIsRunning = false;
    }

    /// <summary>
    /// resets score
    /// </summary>
    void ResetScore()
    {
        SetScore(0);
        interfaceManager.UpdateScore(playerScore);
    }

    /// <summary>
    /// adds score to the player
    /// </summary>
    /// <param name="pointsAdded">how much score to add</param>
    public void SetScore(int s)
    {
        if (interfaceManager != null)
        {
            var sd = GetScore() - s;
            interfaceManager.ScoreDrop(Mathf.Abs(sd));
            playerScore = s;
            interfaceManager.UpdateScore(s);
        }
    }

    /// <summary>
    /// returns player score
    /// </summary>
    /// <returns>playerscore</returns>
    public int GetScore()
    {
        return playerScore;
    }

    /// <summary>
    /// sets collected items interger
    /// </summary>
    /// <param name="c">collectedItems</param>
    public void SetCollectedItems(int c)
    {
        collectedItems = c;
    }

    /// <summary>
    /// returns collected items
    /// </summary>
    /// <returns>collectedItems</returns>
    public int GetCollectedItems()
    {
        return collectedItems;
    }

    /// <summary>
    /// sets player gameObject
    /// </summary>
    /// <param name="p">gameobject parameter</param>
    public void SetMyPlayer(GameObject p)
    {
        myPlayer = p;
    }

    /// <summary>
    /// returns player object returns null if empty
    /// </summary>
    /// <returns>myPlayer gameObject variable</returns>
    public GameObject GetMyPlayer()
    {
        return myPlayer;
    }

    /// <summary>
    /// anything that needs to happen when the game is over
    /// </summary>
    public void EndGame()
    {
        SetDistanceTravelled(myPlayer.transform.position.z - spawnPosition.x);
        LoadScene(2);
    }

    /// <summary>
    /// returns distance travelled
    /// </summary>
    /// <returns></returns>
    public float GetDistance()
    {
        return distanceTraveled;
    }

    /// <summary>
    /// sets distance travelled division by 1.5 to change to "actual" meters
    /// </summary>
    /// <param name="distance">value to set distancetraveled to</param>
    public void SetDistanceTravelled(float distance)
    {
        distanceTraveled = distance / 1.5f;
    }

}