﻿using UnityEngine;
using System.Collections;

/*
 * @author Jared Nealon
 * @author Jan Julius de Lang
 * The Grumpy Cat Team©
 */
/// <summary>
/// represents the absolute bounds of the players location as a gameobject
/// </summary>
[RequireComponent(typeof(BoxCollider))]
public class WallCollider : MonoBehaviour
{

    public bool colliderLeft;

    private Transform targetGameObject;
    private Vector3 colliderSize = new Vector3(1, 20, 30);

    private float nodeSize;

    public void Start()
    {
        targetGameObject = GameObject.FindObjectOfType<Player>().gameObject.transform;
        gameObject.transform.localScale = new Vector3(colliderSize.x, colliderSize.y, colliderSize.z);
        nodeSize = StraightEndlessGenerator.getCurrentNodeSize()/2;
    }

    public void Update()
    {
        if (targetGameObject != null)
        FollowObject(targetGameObject);
    }

    public void FollowObject(Transform target)
    {
        switch (colliderLeft)
        {
            case true:
                transform.position = new Vector3(-nodeSize, 1.5f, target.transform.position.z);
                break;
            case false:
                transform.position = new Vector3(nodeSize, 1.5f, target.transform.position.z);
                break;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>())
        {
            var otherObject = other.GetComponent<Player>();
            otherObject.RespawnPlayer();
        }
    }

}
