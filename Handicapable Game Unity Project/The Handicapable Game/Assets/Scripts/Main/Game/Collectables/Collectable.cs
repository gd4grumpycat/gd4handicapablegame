﻿using UnityEngine;
using System.Collections;

/*
 * @author Jared Nealon
 * @author Jan Julius de Lang
 * The Grumpy Cat Team©
 */
/// <summary>
/// Represents a collectable object Gameobject
/// </summary>
public class Collectable : MonoBehaviour 
{
    private Transform collectibleObject;
    private GameManager gameManager;

    private bool goingUp = true;
    private float hoverDuration = 0.0f;
    public int pointValue = 200;

    private float startPositionY;

    void Start()
    {
        collectibleObject = transform.Find("Object");
        gameManager = GameObject.FindObjectOfType<GameManager>();
        startPositionY = gameObject.transform.position.y;
    }

    void FixedUpdate()
    {
        hoverDuration += Time.deltaTime / 2;

        if (goingUp == true)
            collectibleObject.position = new Vector3(transform.position.x, Mathf.Lerp(startPositionY - 0.25f, startPositionY + 0.25f, hoverDuration), transform.position.z);
        else
            collectibleObject.position = new Vector3(transform.position.x, Mathf.Lerp(startPositionY + 0.25f, startPositionY - 0.25f, hoverDuration), transform.position.z);

        if (hoverDuration >= 1)
            ChangeDirection();

        collectibleObject.RotateAround(Vector3.zero, Vector3.up, 20 * Time.deltaTime);
    }

    /// <summary>
    /// changes direction of the levatation of the collectable object
    /// </summary>
    void ChangeDirection()
    {
        if (goingUp)
            goingUp = false;
        else
            goingUp = true;

        hoverDuration = 0;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>())
        {
            gameManager.SetScore(gameManager.GetScore() + pointValue);
            gameManager.SetCollectedItems(gameManager.GetCollectedItems() + 1);
            gameManager.interfaceManager.UpdatePopup(2);
            Destroy(gameObject);
        }
    }
}
