﻿using UnityEngine;
using System.Collections;

public class AnimalFlying : MonoBehaviour
{

    public bool goUpAndDown;
    public bool butterfly;

    public float hoverDuration = 0.0f;

    [Header("Standard speed for butterfly is 2")]
    public float speed = 2;

    private float moveHeight;


    private bool goingUp = true;

    private float startPositionY;

    void Start()
    {
        startPositionY = transform.position.y;
        if (butterfly)
        {
            transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            transform.eulerAngles = new Vector3(30, Random.Range(-30, 30), 0);
        }
        moveHeight = Random.Range(0.30f, 0.60f);
    }

	void Update () 
    {
	    if (butterfly)
	    {
	        Move(-transform.forward);
	    }
	    else
	    {
	        Move(transform.forward);
	    }
    }

    public void Move(Vector3 dir)
    {
        transform.position += dir * speed * Time.deltaTime;

        hoverDuration += Time.deltaTime / 2;
        if (goUpAndDown)
        {
            if (butterfly)
            {
                if (goingUp == true)
                    transform.position = new Vector3(transform.position.x,
                        Mathf.Lerp(startPositionY - moveHeight, startPositionY + moveHeight, hoverDuration * 2),
                        transform.position.z);
                else
                    transform.position = new Vector3(transform.position.x,
                        Mathf.Lerp(startPositionY + moveHeight, startPositionY - moveHeight, hoverDuration / 2),
                        transform.position.z);
            }
            else
            {
                if (goingUp == true)
                    transform.position = new Vector3(transform.position.x,
                        Mathf.Lerp(startPositionY - moveHeight, startPositionY + moveHeight, hoverDuration),
                        transform.position.z);
                else
                    transform.position = new Vector3(transform.position.x,
                        Mathf.Lerp(startPositionY + moveHeight, startPositionY - moveHeight, hoverDuration),
                        transform.position.z);
            }
            if (hoverDuration >= 1)
                ChangeDirection();
        }
    }

    void ChangeDirection()
    {
        if (goingUp)
            goingUp = false;
        else
            goingUp = true;

        hoverDuration = 0;
    }

}
