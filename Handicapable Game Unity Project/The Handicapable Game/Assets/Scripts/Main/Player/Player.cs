﻿using UnityEngine;
using System.Collections;

/*
 /*
 * @author Jan Julius de Lang
 * @author Jared Nealon
 * The Grumpy Cat Team©
███████▄░░░░▄▄▄▄▄░░░░▄█████████
█████████▄▀▀░░░░░▀▀▀▄██████████
▀█████▀░░░░░░░░░░░░░░▀████████░
░▀██▀░░░░░░░░░░░░░░░░░░░▀████▌░
░░██░░░░░░░░░░░░░░░░░░░░░░███░░
░░█▀░░░░░░░░░░░░░░░░░░░░░░░██░░
░░█░░▄████▄░░░░░▄████▄░░░░░░█░░
░░█░░█▐▄█▐█░░░░░█▐▄█▐█░░░░░░█▄░
░░█░░██▄▄██░░░░░██▄▄██░░░░░░░█░
░▐▌░░░░░░░░░░░░░░░░░░░░░░░░░░▐▌
░▐▌░░░░░░░▀▄▄▄▄▀░░░░░░░░░░░░░▐▌
░▐▌░░░░░░░░░▐▌░░░░░░░░░░░░░░░▐▌
░▐▌░░░░░░░▄▀▀▀▀▄░░░░░░░░░░░░░▐▌
░░█▄░░░░░▀░░░░░░▀░░░░░░░░░░░░█▌
 */
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
//TODO: MAKE COMPATIBLE FOR CAREN SIMULATOR
/// <summary>
/// represents the player that allows it to do everything the player is able to do
/// </summary>
public class Player : MonoBehaviour
{
    //player properties/data
    private int baseMoveSpeed = 5;
    private int moveSpeedMultiplier = 5;
    private int crippledMoveSpeed;
    private float maxSpeed = 5f;

    //other components on the player object that need to be accessed.
    private Rigidbody rigidbody;
    private Camera camera;
    private GameObject cameraObject;
    private GameManager gamemanager;

    private GameManager.GameMode gamemode;


    public void Start()
    {
        LoadComponents();
    }

    public void LoadComponents()
    {
        rigidbody = GetComponent<Rigidbody>();
        camera = GetComponentInChildren<Camera>();
        cameraObject = camera.gameObject;
        gamemanager = GameObject.FindObjectOfType<GameManager>();
    }

    public void Update()
    {
        if (GameManager.gameMode == GameManager.GameMode.AutoWalk)
        {
            Accelerate();
        }
    }

    /// <summary>
    /// accelerates the player along the Z axis
    /// </summary>
    public void Accelerate()
    {
        switch (GameManager.gameMode)
        {
            case GameManager.GameMode.AutoWalk:
                if (rigidbody.velocity.z <= 5)
                {
                    rigidbody.AddForce(Vector3.forward * baseMoveSpeed * moveSpeedMultiplier, ForceMode.Acceleration);
                }
                break;
            case GameManager.GameMode.ManualWalk:
                if (rigidbody.velocity.z <= 5)
                {
                    rigidbody.AddForce(Vector3.forward*baseMoveSpeed*moveSpeedMultiplier, ForceMode.Acceleration);
                }
                break;
            default:

                rigidbody.AddForce(Vector3.forward * baseMoveSpeed * moveSpeedMultiplier, ForceMode.Acceleration);
                break;
        }
    }

    /// <summary>
    /// Accelerates the player along the z axis multiplied by speed value in parameter
    /// </summary>
    /// <param name="speed">multiplied value</param>
    public void Accelerate(float speed)
    {
        rigidbody.AddForce(Vector3.forward*speed*moveSpeedMultiplier, ForceMode.Acceleration);
    }

    /// <summary>
    /// accelerates the player along the z axis multiplied by the speed value parameter and sends the direction as a vector3 as the second paramaeter
    /// </summary>
    /// <param name="speed">multiplied value</param>
    /// <param name="dir">direction as vector3</param>
    public void Accelerate(float speed, Vector3 dir)
    {
        rigidbody.AddForce(dir * speed * moveSpeedMultiplier, ForceMode.Acceleration);
    }

    /// <summary>
    /// leans the player and camera t othe left of right depending on the leanvalue
    /// </summary>
    /// <param name="leanValue">value to lean</param>
    public void Lean(float leanValue)
    {
        if (leanValue <= -0.50f || leanValue >= 0.50f)
        {
            RespawnPlayer();
        }
        else if (leanValue <= -0.05f || leanValue >= 0.05f) 
        {
            rigidbody.AddForce(leanValue * 3.5f * Mathf.Abs(leanValue), 0, 0, ForceMode.Impulse);
        }

        cameraObject.transform.eulerAngles = new Vector3(0, 0, -leanValue * 5);
    }

    /// <summary>
    /// activates respawnplayer in game manager
    /// </summary>
    public void RespawnPlayer()
    {
        gamemanager.RespawnPlayer();
    }

    /// <summary>
    /// sets teh gamemode for the player component incase needed
    /// </summary>
    /// <param name="gm">gamemode type</param>
    public void SetGamemode(GameManager.GameMode gm)
    {
        gamemode = gm;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.name == "Bridge" || other.name == "Rope_Bridge" || other.name == "RopeBridge")
        {
             Handheld.Vibrate();
        }
    }

}
