﻿using UnityEngine;
using System.Collections;

/*
 * @author Jan Julius de Lang
 * @author Jared Nealon
 * The Grumpy Cat Team©
 */
//TODO: MAKE  COMPATIBLE WITH CAREN SIMULATOR
/// <summary>
/// Handles all the input done by the player
/// </summary>
public class PlayerInput : MonoBehaviour
{

    private Player player;

    private InterfaceManager interfaceManager;

    private float valueWalk;

    public void Start()
    {
        player = GetComponent<Player>();
        interfaceManager = GameObject.FindObjectOfType<InterfaceManager>();
    }

    public void Update()
    {
        if (Input.acceleration.y >= .10f)
        {
            player.Accelerate();
        }
        if (Input.GetKey(KeyCode.W))
        {
            player.Accelerate();
        }
        interfaceManager.SetBalanceSliderValue(Input.acceleration.x * 2);

        player.Lean(Input.acceleration.x * 2);
        
#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.A))
        {
            valueWalk -= 0.01f;
        }
        if (Input.GetKey(KeyCode.D))
        {
            valueWalk += 0.01f;
        }
        player.Lean(valueWalk);

        interfaceManager.SetBalanceSliderValue(valueWalk);
        if (Input.GetKey(KeyCode.F))
        {
            player.RespawnPlayer();
        }
#endif
    }
    /// <summary>
    /// returns value walk for testing on computer
    /// </summary>
    /// <returns></returns>
    public float getValueWalk()
    {
        return valueWalk;
    }

    /// <summary>
    /// sets value walk for testing on computer
    /// </summary>
    /// <param name="c"></param>
    public void setValueWalk(float c)
    {
        valueWalk = c;
    }
}

