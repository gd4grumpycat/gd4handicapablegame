﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/*
 * @author Jan Julius de Lang
 * @author Jared Nealon
 * The Grumpy Cat Team©
 */
/// <summary>
/// Generates (unlimited) segments and deletes past segments
/// </summary>
public class StraightEndlessGenerator : MonoBehaviour
{

    /// <summary>
    /// segemnt class holds information for segment
    /// </summary>
    [System.Serializable]
    public class Segment
    {
        public GameObject gameObject;
        public float size = 20;
        public Vector3 rotation;

        public Segment(GameObject prefab, float size)
        {
            this.gameObject = prefab;
            this.size = size;
        }
    }

    //generator settings
    private string generatorName = "segment";
    public bool randomizedGeneration = true;
    public int rerandomizeAfter = 15;
    public Segment[] segmentPrefabList;
    public static int currentNode = 0;
    public static StraightEndlessGenerator myGenerator;

    //events for the class to know if an event has happened
    public static event System.Action OnListGenerated;

    //variables for spawning and reset variables
    public static int generateAhead = 10;
    public static float startingGenerationZ = 0f;

    //static variables and lists
    static Queue<Segment> activeSegments = new Queue<Segment>();
    int previousSegmentIdx;
    Segment[] shuffledSegmentList;
    static float currentSegmentPosition;
    static Segment lastGeneratedSeg;
    static Segment currentPlayerSegment;
    static float currentGenerationZ;

    void Awake()
    {

        //resets static variables if any
        ResetGenerators();

        //shuffles segments in shuffledsegmentslist so the randomized level generate is always different
        ShuffleSegments();

        //resets node counter static variable
        currentNode = 0;

        //loop to generate the first initial segments relative to the generateahead variable
        for (int i = -1; i < generateAhead; i++)
        {
            MoveToNextSegment();
        }
    }


    /// <summary>
    /// shuffles the segments in the segment prefabs segment array
    /// </summary>
    void ShuffleSegments()
    {
        shuffledSegmentList = Util.RandomPermutation<Segment>(segmentPrefabList) as Segment[];
    }


    /// <summary>
    /// returns the generator
    /// </summary>
    /// <returns>current generator</returns>
    public static StraightEndlessGenerator GetGenerator()
    {
        return myGenerator.GetComponent<StraightEndlessGenerator>();
    }


    /// <summary>
    /// instantiates segment, gives name and checks segment size relative to previous segments
    /// </summary>
    /// <param name="index">index of array of segments</param>
    void SpawnSegment(int index)
    {
        Segment segment;
        if (randomizedGeneration)
        {
            segment = shuffledSegmentList[index];
        }
        else
        {
            segment = segmentPrefabList[index];
        }

        GameObject obj = Instantiate(segment.gameObject,
                                     Vector3.forward * currentGenerationZ,
                                     Quaternion.Euler(segment.rotation)) as GameObject;
        Segment seg = new Segment(obj, segment.size);
        activeSegments.Enqueue(seg);
        obj.transform.parent = transform;
        obj.name = "Node" + currentNode + generatorName + "_" + segment.gameObject.name;
        currentNode++;
        lastGeneratedSeg = seg;
        currentGenerationZ += segment.size;
    }


    /// <summary>
    /// returns most recently generated segment as segment object
    /// </summary>
    public static Segment getLastGeneratedSegment
    {
        get
        {
            return lastGeneratedSeg;
        }
    }

    /// <summary>
    /// returns oldest segment alive/first to despawn segment as segment object
    /// </summary>
    public static Segment getOldestAliveSegment
    {
        get
        {
            return currentPlayerSegment;
        }
    }

    public static Vector3 getOldestAlivePosition()
    {
        return currentPlayerSegment.gameObject.transform.position;
    }


    /// <summary>
    /// returns position of node on z axis
    /// </summary>
    public static float getCurrentNodePosition
    {
        get
        {
            return currentSegmentPosition;
        }
    }

    public static float getNextNodePositionZ()
    {
        return currentPlayerSegment.gameObject.transform.position.z +
               currentPlayerSegment.gameObject.transform.localScale.z * 1.5f;
    }

    public static float getCurrentNodeSize()
    {
        return currentPlayerSegment.size;
    }

    /// <summary>
    /// resets the generators values and queue of segments
    /// </summary>
    public static void ResetGenerators()
    {
        activeSegments = new Queue<Segment>();
        myGenerator = Object.FindObjectOfType(typeof(StraightEndlessGenerator)) as StraightEndlessGenerator;
        currentSegmentPosition = startingGenerationZ;
        currentGenerationZ = startingGenerationZ;
    }


    /// <summary>
    /// remove oldest segment and add new segment relative to how far it generates ahead
    /// </summary>
    public void MoveToNextSegment()
    {
        if (activeSegments.Count > 1)
        {
            currentPlayerSegment = activeSegments.ElementAt(1);

            currentSegmentPosition = currentPlayerSegment.gameObject.transform.position.z + currentPlayerSegment.size;
        }

        if (randomizedGeneration)
        {
            if (currentNode == rerandomizeAfter)
            {
                ShuffleSegments();
            }
        }
        //TODO: MAKE PRESET SPAWN SEQUENCES
        SpawnSegment((previousSegmentIdx + 1) % segmentPrefabList.Length);
        previousSegmentIdx++;

        if ((previousSegmentIdx + 1) % segmentPrefabList.Length == 0 && OnListGenerated != null)
        {
            OnListGenerated();
            if (randomizedGeneration)
            {
                ShuffleSegments();
            }
        }


        if (activeSegments.Count >= generateAhead)
        {
            Destroy(activeSegments.Dequeue().gameObject);
        }
    }

    //to test generation
#if UNITY_EDITOR
    void Update()
    {
        if (Input.GetKeyDown("="))
            StraightEndlessGenerator.GetGenerator().MoveToNextSegment();
    }
#endif
}
