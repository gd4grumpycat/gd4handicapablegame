﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
 * @author Jared Nealon
 * @author Jan Julius de Lang
 * The Grumpy Cat Team©
 */
/// <summary>
/// represents the interface manager, manages the ingame interface
/// </summary>
public class InterfaceManager : MonoBehaviour
{
    [SerializeField]private Slider balanceSlider;
    [SerializeField]private Text collectedObjectstText;

    private GameManager gameManager;
    private GameObject myPlayer;

    private int timer = 15 * 60;
    private int startingTime = 15;

    private IEnumerator popupRoutine;
    private IEnumerator timerRoutine;

    private Text popupText;
    private Text scoreText;
    private Text timerText;
    private Text scoreDrop;
    private GameObject tutorialPanel;

    private string[] encouragementStrings = new[] {"Goed werk!", "Je doet het goed!", "Hou het zo!", "Ja!", "Geweldig!", "Niets kan je stoppen!", "Mooi werk!", "Gewoon geweldig!", "Wow!", "Gewoon wow!"};
    private string[] failStrings = new[] {"Niet opgeven!", "Je kunt het wel!", "Kan iedereen gebeuren!", "Goede poging!", "Pas op!", "Let op je balans!", "Gewoon doorzetten!", "Nooit opgeven!", "Je bent maar een mens!", "1 stap per keer!" };
    private string[] collectStrings = new[] {"Je hebt wat gevonden!", "Meer gevonden? Mooi!", "Goed gezien!", "Het is prachtig!", "Nog meer gevonden!", "Ja! Meer!", "Goud!", "Meer verzamelen!", "Verzamel ze allemaal!", "Echte verkenner!" };

    private string[] encouragementStringsEN = new[]
    {
        "Good job!", "You're doing awesome!", "Keep it up!", "Yeah!", "Great!", "Nothing can stop you!", "Good progress!", "Simply amazing!","Wow, you amaze me!","Simply wow!"
    
    };
    private string[] failStringsEN = new[] {"Don't give up!","You can do it!","Can happen to anyone!","Good attempt!","Watch your step!","Mind your balance!","Just keep walking!","Never give up!", "You are only human!","One foot at a time!"
        };
    private string[] collectStringsEN = new[] {"You found some treasure!", "More treasure? Nice!", "You have a good eye!", "You really like shinies!", "Another treasure found!","Hark! More treasure!","There's gold in them!", "You keep collecting them!","Gotta collect them all!","You are a real explorer!"};

    private bool moveButtonPressed = false;

    void Start()
    {
        GrabComponents();
        ResetTimer();
        HidePopup();
        UpdateScore(0);
        HideScoreDrop();
        StartCoroutine(HidingTutorialPopup());
    }

    void Update()
    {
        //used for testing on mobile phone
        if (moveButtonPressed)
        {
            if (myPlayer.GetComponent<Player>().enabled)
            {
                myPlayer.GetComponent<Player>().Accelerate();
            }
        }
    }

    /// <summary>
    /// gets all the components it needs
    /// </summary>
    void GrabComponents()
    {
        popupText = transform.Find("PopUp").GetComponent<Text>();
        scoreDrop = transform.Find("ScoreDrop").GetComponent<Text>();
        scoreText = transform.Find("TimerScore/Score").GetComponent<Text>();
        timerText = transform.Find("TimerScore/Timer").GetComponent<Text>();
        tutorialPanel = transform.Find("Tutorial").gameObject;
        gameManager = GameObject.FindObjectOfType<GameManager>();
        myPlayer = GameObject.FindObjectOfType<Player>().gameObject;
    }
    
    /// <summary>
    /// resets the timer to starting time
    /// </summary>
    void ResetTimer()
    {
        timer = startingTime * 60;
        UpdateTimer();
    }

    /// <summary>
    /// updates the timer
    /// </summary>
    void UpdateTimer()
    {
        if (timer <= 0)
        {
            gameManager.EndGame();
        }
        timer--;
        timerText.text = "TIMER - " + FormatTime();
        WaitOneSecond();
    }

    /// <summary>
    /// formats the time to 0:00
    /// </summary>
    /// <returns>returns string of correct format</returns>
    string FormatTime()
    {
        int minutes = Mathf.FloorToInt(timer / 60);
        int seconds = Mathf.FloorToInt(timer - minutes * 60);
        return string.Format("{0:0}:{1:00}", minutes, seconds);
    }

    /// <summary>
    /// timer to wait one second
    /// </summary>
    void WaitOneSecond()
    {
        if (timerRoutine != null)
            StopCoroutine(timerRoutine);

        timerRoutine = WaitingOneSecond();

        StartCoroutine(timerRoutine);
    }

    /// <summary>
    /// updates the timer after one second
    /// </summary>
    /// <returns></returns>
    IEnumerator WaitingOneSecond()
    {
        yield return new WaitForSeconds(1);
        UpdateTimer();
        // UpdatePopup(Random.Range(0, 2)); // Test Popups
    }

   /// <summary>
   /// updates the score text on the screen
   /// </summary>
   /// <param name="newScore">score value</param>
    public void UpdateScore(int newScore)
    {
        if (scoreText == null)
            GrabComponents();

        scoreText.text = "SCORE - " + newScore + " punten";
    }

    public void ScoreDrop(int value)
    {
        if (scoreDrop != null)
        {
            scoreDrop.gameObject.SetActive(true);
            scoreDrop.text = "+" + value.ToString();
            StartCoroutine("ScoreDropSetter");
        }
    }

    /// <summary>
    /// sends a popup
    /// </summary>
    /// <param name="type">popup type 0 = encouragement, 1 = fail, 2 = item collected</param>
    public void UpdatePopup(int type)
    {
        var r = Random.Range(0, 100);
        if (r >= 75)
        {
            switch (type)
            {
                case 0:
                    UpdatePopupText(EncouragementPass());
                    break;
                case 1:
                    UpdatePopupText(EncouragementFail());
                    break;
                case 2:
                    UpdatePopupText(EncouragementCollect());
                    break;
            }
        }
    }

    void UpdatePopupText(string newPopup)
    {
        popupText.text = newPopup;
        ShowPopup();
        WaitToHide();
    }

    /// <summary>
    /// returns a random encourangement string
    /// </summary>
    /// <returns></returns>
    string EncouragementPass()
    {
        string popupText = "";
        int random = Random.Range(0, encouragementStrings.Length);
        switch (Settings.language)
        {
            case Settings.Language.Dutch:
                popupText = encouragementStrings[random];
                break;
            case Settings.Language.English:
                popupText = encouragementStringsEN[random];
                break;
        }
        return popupText;
    }

    /// <summary>
    /// returns a random fail string
    /// </summary>
    /// <returns></returns>
    string EncouragementFail()
    {
        string popupText = "";
        int random = Random.Range(0, failStrings.Length);
        switch (Settings.language)
        {
            case Settings.Language.Dutch:
                popupText = failStrings[random];
                break;
            case Settings.Language.English:
                popupText = failStringsEN[random];
                break;
        }
        return popupText;
    }

    /// <summary>
    /// returns a random collected object string
    /// </summary>
    /// <returns></returns>
    string EncouragementCollect()
    {
        string popupText = "";
        int random = Random.Range(0, collectStrings.Length);
        switch (Settings.language)
        {
            case Settings.Language.Dutch:
                popupText = collectStrings[random];
                break;
            case Settings.Language.English:
                popupText = collectStringsEN[random];
                break;
        }
        return popupText;
    }

    /// <summary>
    /// shows the popup with a random text color
    /// </summary>
    void ShowPopup()
    {
        popupText.color = new Color(Random.Range(0.0f, 0.9f), Random.Range(0.0f, 0.9f), Random.Range(0.0f, 0.9f), 1);
    }

    /// <summary>
    /// hides the popup by setting alpha to 0
    /// </summary>
    void HidePopup()
    {
        popupText.color = new Color(0, 0, 0, 0);
    }

    void WaitToHide()
    {
        if (popupRoutine != null)
            StopCoroutine(popupRoutine);

        popupRoutine = WaitingToHide();

        StartCoroutine(popupRoutine);
    }

    IEnumerator WaitingToHide()
    {
        yield return new WaitForSeconds(2);
        HidePopup();
    }

    IEnumerator ScoreDropSetter()
    {
        yield return new WaitForSeconds(0.5f);
        HideScoreDrop();
    }

    void HideScoreDrop()
    {
        scoreDrop.gameObject.SetActive(false);
    }

    /// <summary>
    /// sets the move button pressed to value b
    /// </summary>
    /// <param name="b"></param>
    /// testing purposes only
    public void SetMoveButtonPressed(bool b)
    {
        moveButtonPressed = b;
    }

    /// <summary>
    /// returns bool if button is presssed
    /// </summary>
    /// <returns></returns>
    /// testing purposes only
    public bool GetMoveButtonPressed()
    {
        return moveButtonPressed;
    }

    /// <summary>
    /// sets the balance slider on the itnerfacemanager using value in parameter
    /// </summary>
    /// <param name="value">value to set balanceslider to</param>
    public void SetBalanceSliderValue(float value)
    {
        balanceSlider.value = value;
    }

    /// <summary>
    /// sets collected object by string parameter
    /// </summary>
    /// <param name="txt"></param>
    public void SetCollectedObjects(string txt)
    {
        collectedObjectstText.text = txt;
    }

    /// <summary>
    /// sets collected objects from int value into string 
    /// </summary>
    /// <param name="value"></param>
    public void SetCollectedObjects(int value)
    {
        collectedObjectstText.text = value.ToString();
    }

    void HideTutorialPopup()
    {
        tutorialPanel.SetActive(false);
    }

    IEnumerator HidingTutorialPopup()
    {
        yield return new WaitForSeconds(20);
        HideTutorialPopup();
    }
}
