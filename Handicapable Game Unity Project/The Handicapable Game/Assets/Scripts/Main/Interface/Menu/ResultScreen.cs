﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class ResultScreen : MonoBehaviour
{
    private GameManager gameManager;

    private Text distanceResult;
    private Text treasureResult;
    private Text scoreResult;
    private Text timer;

	// Use this for initialization
	void Start ()
    {
        GrabManager();
        GrabComponents();
        GrabResults();
        StartCoroutine(ReturnToMainMenu());
    }

    void GrabManager()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();
    }

    void GrabComponents()
    {
        distanceResult = transform.Find("DistanceResult").GetComponent<Text>();
        treasureResult = transform.Find("TreasureResult").GetComponent<Text>();
        scoreResult = transform.Find("ScoreResult").GetComponent<Text>();
        timer = transform.Find("Timer").GetComponent<Text>();
    }

    void GrabResults()
    {
        distanceResult.text = gameManager.GetDistance().ToString() + " m";
        treasureResult.text = gameManager.GetCollectedItems().ToString();
        scoreResult.text = gameManager.GetScore().ToString() + " punten";
    }

    IEnumerator ReturnToMainMenu()
    {
        timer.text = "5";
        yield return new WaitForSeconds(1);
        timer.text = "4";
        yield return new WaitForSeconds(1);
        timer.text = "3";
        yield return new WaitForSeconds(1);
        timer.text = "2";
        yield return new WaitForSeconds(1);
        timer.text = "1";
        yield return new WaitForSeconds(1);
        timer.text = "TOT ZIENS!";
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(0);
    }
}
