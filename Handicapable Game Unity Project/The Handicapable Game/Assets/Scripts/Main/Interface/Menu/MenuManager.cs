﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
 * @author Jan Julius de Lang
 * @author Jared Nealon
 * The Grumpy Cat Team©
 */
/// <summary>
/// handles the menu's and interfaces
/// </summary>
public class MenuManager : MonoBehaviour
{
    public GameManager gamemanager;

    public void Start()
    {
        gamemanager = GameObject.FindObjectOfType<GameManager>();
    }

    public void Update()
    {
        //TODO: CAREN SIMULATOR INTERGRATION
        if (Input.acceleration.x <= -0.5f)
        {
            StartGameButton(0);
        }
        else if (Input.acceleration.x >= 0.5f)
        {
            StartGameButton(1);
        }
    }

    public void StartGameButton(int gamemodeId)
    {
        gamemanager.StartGame(gamemodeId);   
    }
}
